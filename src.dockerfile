FROM gogs-ubuntu

LABEL maintainer="Jonathan Vagnier <adm.jvagnier@gmail.com>"

COPY ./src /tmp/src

RUN /usr/libexec/s2i/assemble