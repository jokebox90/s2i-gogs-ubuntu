
IMAGE_NAME = gogs-ubuntu

.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .

.PHONY: test
test:
	docker build -t $(IMAGE_NAME)-candidate .
	IMAGE_NAME=$(IMAGE_NAME)-candidate test/run

.PHONY: install
install:
	cd src; \
	s2i build . $(IMAGE_NAME) $(IMAGE_NAME)-app

.PHONY: run
run:
	docker run -d -p 3000:3000 --name $(IMAGE_NAME)-run $(IMAGE_NAME)-app

.PHONY: clean
clean:
	docker rm -f $(IMAGE_NAME)-run

.PHONY: reset
reset:
	docker rm -f $(IMAGE_NAME)-run
	docker rmi $(IMAGE_NAME)
