FROM phusion/baseimage

LABEL maintainer="Jonathan Vagnier <adm.jvagnier@gmail.com>"

# Set the labels that are used for OpenShift to describe the builder image.
LABEL io.k8s.description="Gogs on Ubuntu image" \
    io.k8s.display-name="Gogs Ubuntu" \
    io.openshift.expose-services="3000:http" \
    io.openshift.tags="builder,webserver,html,proxy,ubuntu,gogs" \
    # this label tells s2i where to find its mandatory scripts
    # (run, assemble, save-artifacts)
    io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

# Package installation and cache cleanup
ENV DEBIAN_FRONTEND noninteractive

# Gogs SCM installation
RUN addgroup --gid 1001 git && \
    adduser \
    --disabled-password \
    --uid 1001 \
    --gid 1001 \
    --shell /bin/bash \
    --home /app \
    --gecos "Git SCM" \
    git

RUN INSTALL_PKGS="wget git sqlite3" && \
    # APT install
    apt-get update && \
    apt-get install -y \
        --no-install-suggests \
        --no-install-recommends \
        ${INSTALL_PKGS} && \
    # APT cleanup
    apt-get clean && \
    apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* \
           /var/cache/apt/archives/*

# Other utilities installation
ENV DOCKERIZE_VERSION v0.6.1

RUN wget "https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" && \
    tar -C "/usr/local/bin" -xzvf "dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz" && \
    rm -vf "dockerize-linux-amd64-${DOCKERIZE_VERSION}.tar.gz"

COPY .s2i/bin /usr/libexec/s2i
RUN find /usr/libexec/s2i -type f -print | xargs chmod +x

USER 1001
WORKDIR /app

ENV GOGS_VERSION 0.11.86

RUN wget "https://dl.gogs.io/${GOGS_VERSION}/gogs_${GOGS_VERSION}_linux_amd64.tar.gz" && \
    tar -xzvf "gogs_${GOGS_VERSION}_linux_amd64.tar.gz" && \
    rm -vf "gogs_${GOGS_VERSION}_linux_amd64.tar.gz"

EXPOSE 3000 22

CMD [ "/usr/libexec/s2i/run" ]
